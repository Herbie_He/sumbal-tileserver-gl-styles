This repo uses styles from https://gitlab.com/abdullahsumbal/sumbaltileserver-gl-styles
# TileServer-GL styles

This repository publishes npm package called `sumbal-tileserver-gl-styles` which contains map styles and fonts. 


### Steps to publish

* First log in npm `npm login`. 

* Run 'node publish.js'. This will create an npm package called `sumbal-tileserver-gl-styles`.

Note: Make sure to update the version number in package.json file. 

### Steps to add new style

* Add style name directory in styles_modules (I added sumbal-basic)

* Add a file called url inside sumbal-basic with an url linking to zip file. (for e.g my url links to [gitlab repo](https://gitlab.com/abdullahsumbal/sumbal-style) tag https://gitlab.com/abdullahsumbal/sumbal-style/-/archive/v1/sumbal-style-v1.zip) 

* You may need to make changes in the publish.js file depending on how your zip file is structured.  
